$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 3000
    });
    $('#contacto1').on('show.bs.modal', function (e){
      $('#contactoBtn1').removeClass('btn-outline-success');
      $('#contactoBtn1').addClass('btn-primary');
      $('#contactoBtn1').prop('disabled', true);
      console.log('El modal contacto se está mostrando');
    });
    $('#contacto').on('shown.bs.modal', function (e){
      console.log('El modal contacto se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e){
      console.log('El modal contacto se está cerrando');
    });
    $('#contacto1').on('hidden.bs.modal', function (e){
      $('#contactoBtn1').removeClass('btn-primary');
      $('#contactoBtn1').addClass('btn-outline-secondary');
      $('#contactoBtn1').prop('disabled', false);
      console.log('El modal contacto se cerró');
    });
    $('#contacto2').on('show.bs.modal', function (e){
      $('#contactoBtn2').removeClass('btn-outline-success');
      $('#contactoBtn2').addClass('btn-primary');
      $('#contactoBtn2').prop('disabled', true);
      console.log('El modal contacto se está mostrando');
    });
    $('#contacto2').on('hidden.bs.modal', function (e){
      $('#contactoBtn2').removeClass('btn-primary');
      $('#contactoBtn2').addClass('btn-outline-secondary');
      $('#contactoBtn2').prop('disabled', false);
      console.log('El modal contacto se cerró');
    });
    $('#contacto3').on('show.bs.modal', function (e){
      $('#contactoBtn3').removeClass('btn-outline-success');
      $('#contactoBtn3').addClass('btn-primary');
      $('#contactoBtn3').prop('disabled', true);
      console.log('El modal contacto se está mostrando');
    });
    $('#contacto3').on('hidden.bs.modal', function (e){
      $('#contactoBtn3').removeClass('btn-primary');
      $('#contactoBtn3').addClass('btn-outline-secondary');
      $('#contactoBtn3').prop('disabled', false);
      console.log('El modal contacto se cerró');
    });
    $('#contacto4').on('show.bs.modal', function (e){
      $('#contactoBtn4').removeClass('btn-outline-success');
      $('#contactoBtn4').addClass('btn-primary');
      $('#contactoBtn4').prop('disabled', true);
      console.log('El modal contacto se está mostrando');
    });
    $('#contacto4').on('hidden.bs.modal', function (e){
      $('#contactoBtn4').removeClass('btn-primary');
      $('#contactoBtn4').addClass('btn-outline-secondary');
      $('#contactoBtn4').prop('disabled', false);
      console.log('El modal contacto se cerró');
    });
    $('#contacto5').on('show.bs.modal', function (e){
      $('#contactoBtn5').removeClass('btn-outline-success');
      $('#contactoBtn5').addClass('btn-primary');
      $('#contactoBtn5').prop('disabled', true);
      console.log('El modal contacto se está mostrando');
    });
    $('#contacto5').on('hidden.bs.modal', function (e){
      $('#contactoBtn5').removeClass('btn-primary');
      $('#contactoBtn5').addClass('btn-outline-secondary');
      $('#contactoBtn5').prop('disabled', false);
      console.log('El modal contacto se cerró');
    });
    $('#contacto6').on('show.bs.modal', function (e){
      $('#contactoBtn6').removeClass('btn-outline-success');
      $('#contactoBtn6').addClass('btn-primary');
      $('#contactoBtn6').prop('disabled', true);
      console.log('El modal contacto se está mostrando');
    });
    $('#contacto6').on('hidden.bs.modal', function (e){
      $('#contactoBtn6').removeClass('btn-primary');
      $('#contactoBtn6').addClass('btn-outline-secondary');
      $('#contactoBtn6').prop('disabled', false);
      console.log('El modal contacto se cerró');
    });
    $('#contacto7').on('show.bs.modal', function (e){
      $('#contactoBtn7').removeClass('btn-outline-success');
      $('#contactoBtn7').addClass('btn-primary');
      $('#contactoBtn7').prop('disabled', true);
      console.log('El modal contacto se está mostrando');
    });
    $('#contacto7').on('hidden.bs.modal', function (e){
      $('#contactoBtn7').removeClass('btn-primary');
      $('#contactoBtn7').addClass('btn-outline-secondary');
      $('#contactoBtn7').prop('disabled', false);
      console.log('El modal contacto se cerró');
    });
});